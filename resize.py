import os, sys
from PIL import Image

size = 1028, 1028
instagram = 'instagram'

def make_new_dir(path_to_folder=None):
    print(path_to_folder)
    p = path_to_folder + instagram
    if not os.path.exists(p):
        print('Folder does not exist')
        os.makedirs(p)
    else:
        print('Folder exists. Deleting and creating a new folder')
        import shutil
        shutil.rmtree(p)
        os.makedirs(p)


def resize_image(background_color, files_directory=None):
    if files_directory == None:
        files_directory = os.getcwd()

    for infile in os.listdir(files_directory):
        if infile.endswith('.jpg'):
            outfile = os.path.splitext(infile)[0] + ".JPEG"
            if background_color == 'b':
                background = Image.new("RGBA", size, "black" )
            else:
                background = Image.new("RGBA", size, "white" )

            try:
                img = Image.open(files_directory + '/' + infile)
                img.thumbnail(size, Image.ANTIALIAS)
                img_w, img_h = img.size

                bg_w, bg_h = background.size
                offset = ((bg_w - img_w) / 2, (bg_h - img_h) / 2)
                background.paste(img, offset)
                background.save(files_directory + instagram + '/' + outfile)
            except IOError, e:
                print(str(e))
                print "cannot create image for '%s'" % infile


def main(argv):
    if len(argv) == 2:
        make_new_dir(argv[0])
        resize_image(argv[1], argv[0])
    else:
        resize_image(argv[0])
    print('Done!')


if __name__ == "__main__":
    main(sys.argv[1:])
